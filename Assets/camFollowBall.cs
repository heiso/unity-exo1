﻿using UnityEngine;
using System.Collections;

public class camFollowBall : MonoBehaviour {
	
	public GameObject objectToFollow;
	
	// object position
	private Vector3 objectPos;
	
	// camera positions
	private Vector3 camPos;
	private Vector3 lastCamPos;
	
	
	private float step = 10;
	
	// animations attributes
	private float speed = 10;
	private float startTime;
	private bool isAnimationRunning;

	// Use this for initialization
	void Start () {
		isAnimationRunning = false;
	}
	
	// Update is called once per frame
	void Update () {
		objectPos = objectToFollow.transform.position;
		camPos = transform.position;
		
		if(Mathf.Abs(objectPos.x - camPos.x) > step || isAnimationRunning) {
			if(!isAnimationRunning) {
				isAnimationRunning = true;
				startTime = Time.time;
				lastCamPos = camPos;
			}
			
			float dist = Vector3.Distance(new Vector3(lastCamPos.x, 0, 0), new Vector3(objectPos.x, 0, 0));
			float distCovered = (Time.time - startTime) * speed;
			float distPercent = distCovered/dist;
			transform.position = Vector3.Lerp(lastCamPos, new Vector3(objectPos.x, lastCamPos.y, lastCamPos.z), distPercent);
			
			if(distPercent >= 1) {
				isAnimationRunning = false;
			}
		}
			
		
		transform.LookAt(objectToFollow.transform);
	}
}
