﻿using UnityEngine;
using System.Collections;

public class ballControl : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKey(KeyCode.RightArrow)) {
			rigidbody.AddForce(Vector3.right);
		}
		if(Input.GetKey(KeyCode.LeftArrow)) {
			rigidbody.AddForce(Vector3.left);
		}
		if(Input.GetKey(KeyCode.UpArrow)) {
			rigidbody.AddForce(Vector3.forward);
		}
		if(Input.GetKey(KeyCode.DownArrow)) {
			rigidbody.AddForce(Vector3.back);
		}
		if(Input.GetKey(KeyCode.Space)) {
			rigidbody.AddForce(Vector3.up, ForceMode.Impulse);
		}
	}
}
